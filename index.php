<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>input form</title>
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="registration-form">
                    <form action="hasil.php" method="post">
                        <div class="text-center pb-2">
                            <h2>Input Data Mahasiswa</h2>
                        </div>
                        <div>
                            <input type="text" class="form-control item" name="nama" id="nama" placeholder="Nama"required>
                        </div>
                        <div>
                            <input type="number" class="form-control item" name="nim" id="nim" placeholder="NIM"required>
                        </div>
                        <div>
                            <input type="text" class="form-control item" name="mapel" id="mapel"
                                placeholder="Mata Pelajaran"required>
                        </div>
                        <div>
                            <input type="number" class="form-control item" name="uas" id="uas" min="0" max="100"
                                placeholder="Nilai UAS"required>
                        </div>
                        <div>
                            <input type="number" class="form-control item" name="uts" id="uts" min="0" max="100"
                                placeholder="Nilai UTS" required>
                        </div>
                        <div>
                            <input type="number" class="form-control item" name="tgs" id="tgs" min="0" max="100"
                                placeholder="Nilai Tugas" required>
                        </div>
                        <div>
                            <button type="sumbit" class="btn btn-block button">Hitung</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

</body>

</html>