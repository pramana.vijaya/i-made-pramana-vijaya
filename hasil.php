<?php
  $nama = $_POST['nama'];
  $nim = $_POST['nim'];
  $mapel = $_POST['mapel'];
  $uas = $_POST['uas'];
  $uts = $_POST['uts'];
  $tgs = $_POST['tgs'];

  $nilai_uas= $uas * 0.5 ;
  $nilai_uts= $uts * 0.35;
  $nilai_tgs= $tgs * 0.15 ;

$nilai_akhir= $nilai_uas + $nilai_uts + $nilai_tgs;


if ($nilai_akhir >= 90)
{
$grade = "A";
}

elseif ($nilai_akhir >= 70 )

{

$grade = "B";

}

elseif ($nilai_akhir >= 50 )

{

$grade = "C";

}

elseif ($nilai_akhir <= 50)

{

$grade = "D";


}


?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>hasil</title>
</head>

<body>
    <div class="container">
    <div class="registration-form">
    <div class="tabel" >
         <h2>Hasil nilai Mahasiswa</h2>
                            <table class="table table-bordered bg-white border-primary" >
                                <thead>
                                    <tr>
                                        <th scope="col">Nama</th>
                                        <th scope="col">NIM</th>
                                        <th scope="col">Mata Pelajaran</th>
                                        <th scope="col">Nilai UTS</th>
                                        <th scope="col">Nilai UAS</th>
                                        <th scope="col">Nilai Tugas</th>
                                        <th scope="col">Grade</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th><?php echo $nama?></th>
                                        <th><?php echo $nim?></th>
                                        <th><?php echo $mapel?></th>
                                        <th><?php echo $uts?></th>
                                        <th><?php echo $uas?></th>
                                        <th><?php echo $tgs?></th>
                                        <th><?php echo $grade?></th>
                                        <th><?php echo $nilai_akhir ?></th>
                                    </tr>
                                </tbody>
                            </table>
             

    </div>
    </div>
    </div>


            <!-- Optional JavaScript; choose one of the two! -->

            <!-- Option 1: Bootstrap Bundle with Popper -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
                crossorigin="anonymous">
            </script>


</body>

</html>